var notifier = require('node-notifier');
var path = require('path');
var util = require('gulp-util');

/* --- Error handler --- */

module.exports = function(err){

  if(err.fileName !== undefined){
    var filepath = path.normalize(err.fileName);
    var file = path.basename(filepath);

    notifier.notify({
      title: err.plugin,
      message: err.message + "\n" + file + ', line ' + err.lineNumber,
      sound: false
    });
  }
  else {
    notifier.notify({
      title: err.plugin,
      message: err.message + ', line ' + err.lineNumber,
      sound: false
    });
  }

  util.beep();

  util.log(util.colors.gray('--------------'));
  util.log(util.colors.red(err.plugin+':'), util.colors.gray(err.message));
  if(filepath !== undefined && err.lineNumber !== undefined){
    util.log(filepath.replace(__dirname,'') + ' (Line '+err.lineNumber+')')
  }
  util.log(util.colors.gray('--------------'));

  this.emit('end');
  if(typeof this.end == 'function'){
    return this.end();
  }

  return;
}
