'use strict';

var gulp = require('gulp');
var fs = require('fs');
var shell = require('gulp-shell');
var plugins = require('gulp-load-plugins')();
plugins.browserSync = require('browser-sync').create();

var config = {};

var production = (plugins.util.env.production == "true") || false;

var paths = {
  css: {
    public: ['./src/css/*.css'],
    watch: ['./src/css/**/*.*','./src/css/**/**/**'],
    output: './public/styles/'
  },
  scripts: {
    public: {
      app: './src/js/app.js',
      inline: './src/js/inline.js'
    },
    watch: './src/js/**/**.js',
    output: './public/js'
  },
  svg: {
    sprite: {
      in: './src/svg/sprite/*.svg',
      out: './public/svg'
    },
    inline: {
      in: './src/svg/inline/*.svg',
      out: './public/svg/inline'
    },
    watch: ['./src/svg/**/*.svg']
  },
  image: {
    in: './src/img/*.jpg',
    out: './public/images'
  },
  views: {
    watch: './templates/views/**/*.hbs'
  }
};

// Tasks
fs.readdirSync(__dirname + '/gulp').forEach(function(file) {
  if(file.charAt(0) !== '_'){
    var moduleName = file.replace('.js','');
    require('./gulp/' + file)(gulp, plugins, production, paths[moduleName]);
  }
});

var buildTasks = ['css','buildScripts','svg','inlinesvg'];
var watchTasks = ['css','watchScripts','svg','inlinesvg'];
var watchPost = ['init', 'watch'];

gulp.task('init', gulp.series(function(done){

  plugins.browserSync.init({
    proxy: 'http://localhost:3000',
    host: '<%= slug %>',
    port: '4000',
    open: false,
  }, done);

}));

gulp.task('watch', function(){
  gulp.watch(paths.css.watch, gulp.parallel('css'));
  gulp.watch(paths.svg.watch, gulp.parallel('svg','inlinesvg'));
  gulp.watch(paths.image.in, gulp.parallel('image'));
  gulp.watch(paths.views.watch, function(){ plugins.browserSync.reload() });
});

gulp.task('default', gulp.parallel(gulp.parallel(watchTasks), watchPost));
gulp.task('build', gulp.series(buildTasks));
