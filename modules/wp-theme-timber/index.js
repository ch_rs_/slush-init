var gulp = require('gulp');
var async = require('async');
var plugins = require('gulp-load-plugins')();

module.exports = {
  name: "wp-theme-timber",
  description: "Timber for WordPress",
  group: "02-cms-theme",
  prompts: []
}
