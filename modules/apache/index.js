var gulp = require('gulp');
var async = require('async');
var plugins = require('gulp-load-plugins')();

var ignoreFiles = ['.DS_Store'];

module.exports = {
  name: "apache",
  description: "Files for an Apache server",
  group: "06-extras",
  prompts: []
}
