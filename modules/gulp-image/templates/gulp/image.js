var error = require('./_error.js');
var jpeg = require('imagemin-jpeg-recompress');
var webp = require('imagemin-webp');

module.exports = function(gulp, $, production, paths){

  gulp.task('image-jpeg', gulp.series(function() {
    return gulp.src(paths.in)
    .pipe($.imagemin([
      jpeg({
          quality: "medium"
      })
    ]))
    .pipe(gulp.dest(paths.out));
  }));

  gulp.task('image-webp', gulp.series(function() {
    return gulp.src(paths.in)
    .pipe($.imagemin([
      webp({
        quality: 50
      })
    ]))
    .pipe($.extReplace('webp'))
    .pipe(gulp.dest(paths.out));
  }));

  gulp.task('image', gulp.parallel('image-jpeg', 'image-webp'));

}
