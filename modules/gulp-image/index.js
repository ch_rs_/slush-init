var gulp = require('gulp');
var async = require('async');
var plugins = require('gulp-load-plugins')();

module.exports = {
  name: "gulp-image",
  description: "JPEG and WebP Gulp task",
  group: "05-gulp-general",
  prompts: [],
  default: true,
  devDependencies: {
    "gulp-imagemin": "^3.0.3",
    "imagemin-jpeg-recompress": "^5.1.0",
    "imagemin-webp": "^4.0.0"
  }
}
