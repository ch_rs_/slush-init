var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

module.exports = {
  name: "gulp-svg",
  description: "SVG inline and sprite Gulp task",
  group: "05-gulp-general",
  prompts: [],
  default: true,
  devDependencies: {
    "svg4everybody": "^2.1.0",
    "gulp-svg-sprite": "^1.3.6",
    "gulp-svgmin": "^1.2.3"
  }
}
