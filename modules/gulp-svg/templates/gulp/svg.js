var error = require('./_error.js');

module.exports = function(gulp, $, production, paths){

  gulp.task('svg', function () {
    return gulp.src(paths.sprite.in)
      .pipe($.svgSprite({
        mode: {
          symbol: {
            prefix: "svg-%s"
          }
        }
      }))
      .on('error', error)
      .pipe($.size({title:"SVG sprite"}))
      .pipe(gulp.dest(paths.sprite.out));
  });

  gulp.task('inlinesvg', function () {
    return gulp.src(paths.inline.in)
      .pipe($.svgmin())
      .on('error', error)
      .pipe($.extReplace('.hbs'))
      .pipe($.size({title:"Inline SVG"}))
      .pipe(gulp.dest(paths.inline.out));
  });

}
