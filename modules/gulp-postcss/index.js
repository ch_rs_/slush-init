var gulp = require('gulp');
var async = require('async');
var plugins = require('gulp-load-plugins')();

module.exports = {
  name: "gulp-postcss",
  description: "PostCSS base and Gulp task",
  group: "03-styles",
  prompts: [],
  default: true,
  devDependencies: {
    "gulp-postcss": "^6.1.1",
    "lost": "^7.1.0",
    "node-sass": "^3.9.3",
    "node-sass-middleware": "^0.9.8",
    "postcss-cssnext": "^2.7.0",
    "postcss-easy-import": "^1.0.1",
    "postcss-reporter": "latest",
    "postcss-scss": "^0.2.1",
    "stylelint": "^7.8.0",
    "stylelint-config-standard": "^16.0.0",
    "stylelint-config-lost": "^0.0.2",
    "gulp-cssnano": "^2.1.2"
  }
}
