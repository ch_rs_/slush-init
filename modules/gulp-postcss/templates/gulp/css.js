var error = require('./_error.js');

var browsers = [
  "last 2 versions"
];

var sizeOpts = {
  showFiles: true,
  gzip: true,
  title:"CSS"
};

var processors = [
  require('postcss-easy-import')({
    extensions: ".css",
    plugins: [
      require('stylelint')()
    ]
  }),
  require('postcss-cssnext')({
    browsers: "last 2 versions, > 5% in GB, ie >= 10",
    features:{
      rem:{
        rootValue: '10px'
      }
    }
  }),
  require('lost'),
  require('postcss-reporter')({
    clearAllMessages: true
  })
];


module.exports = function (gulp, $, production, paths) {

  gulp.task('css', function(){
    return gulp.src(paths.public)
      .pipe($.if(!production,$.sourcemaps.init()))
      .pipe($.postcss(processors))
      .on('error', error)
      .pipe($.cssnano())
      .pipe($.if(!production,$.sourcemaps.write()))
      .pipe(gulp.dest(paths.output))
      .pipe($.size(sizeOpts))
      .pipe($.buster())
      .pipe(gulp.dest('.'))
      .pipe($.browserSync.stream());
  });
};
