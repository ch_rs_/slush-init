var gulp = require('gulp');
var async = require('async');
var plugins = require('gulp-load-plugins')();

module.exports = {
  name: "docker",
  description: "Docker config files",
  group: "06-extras",
  default: true,
  prompts: []
}
