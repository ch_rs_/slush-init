var gulp = require('gulp');
var async = require('async');
var plugins = require('gulp-load-plugins')();
var childProcess = require('child_process');

module.exports = {
  name: "yarn-npm",
  description: "Run Yarn or NPM",
  group: "06-extras",
  templatesByDefault: false,
  default: true,
  prompts: [{
      type: 'list',
      name: 'yarnNpm',
      message: 'Package manager to run',
      choices: ['yarn', 'npm install']
  }],
  extraTasks: function(answers){

    console.log('Running ' + answers.yarnNpm);

    var command = answers.yarnNpm.split(' ');

    var spawn = require('child_process').spawn;
    var child = spawn(command.shift(), command);

    child.stdout.on('data', function(data){
      console.log('' + data);
    });

    child.stderr.on('data', function(data){
      console.log('' + data);
    });

    child.on('close', (code) => {
      console.log(answers.yarnNpm + ' finished');
    });
  }
}
