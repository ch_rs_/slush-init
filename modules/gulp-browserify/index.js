var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

module.exports = {
  name: "gulp-browserify",
  description: "Browserify JS Gulp task",
  group: "04-scripts",
  prompts: [],
  default: true,
  devDependencies: {
    "browserify": "^13.1.0",
    "vinyl-buffer": "^1.0.0",
    "vinyl-fs": "^2.4.3",
    "vinyl-source-stream": "^1.1.0",
    "vinyl-transform": "^1.0.0",
    "watchify": "^3.7.0"
  }
}
