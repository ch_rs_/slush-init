//----------------------------------*\
// Add slidey thing to navigation
//----------------------------------*\

var navHover = module.exports = function(element,options){

    this.element = document.querySelector(element);

    // Check for target element
    if( this.element == null ) return;

    // Create highlight element
    this.highlight = document.createElement("li");
    this.highlight.classList.add('nav__highlight');

    this.setInitial();

    this.element.appendChild(this.highlight);

    this.mouseEvents();

    window.onresize = function(){
      this.setInitial();
    }.bind(this);
};

navHover.prototype.mouseEvents = function(){

  var navItems = this.element.querySelectorAll("li:not(.nav__highlight)");
  [].forEach.call(navItems, function(item){

    item.addEventListener("mouseenter", function(e){
      this.highlight.style.width = item.offsetWidth + "px";
      this.highlight.style.transform = "translate3d(" + item.offsetLeft + "px, 0, 0)";
    }.bind(this));

    item.addEventListener("mouseleave", function(e){
      this.highlight.style.width = this.initialWidth + "px";
      this.highlight.style.transform = "translate3d(" + this.initialOffset + "px, 0, 0)";
    }.bind(this));

  }.bind(this));
}

navHover.prototype.setInitial = function(){
  this.initialOffset = this.element.querySelector("li.active").offsetLeft;
  this.initialWidth = this.element.querySelector("li.active").offsetWidth;
  this.highlight.style.width = this.initialWidth + "px";
  this.highlight.style.transform = "translate3d(" + this.initialOffset + "px, 0, 0)";
  return;
};
