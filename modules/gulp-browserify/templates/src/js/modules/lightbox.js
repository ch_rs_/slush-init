//----------------------------------*\
// Lightbox
//----------------------------------*\

module.exports = function(element,options){

    if(typeof jQuery == "undefined") return;
    this.element = $(element);

    // Check for target element
    if( this.element.length < 1 ) return;

    $(this.element).lightGallery({
      selector: '[data-lightbox]',
      toogleThumb: false
    });

    $('[data-click]').click(function(e){
      e.preventDefault();

      var target = $(e.currentTarget).attr('data-click');
      $(target).click();
    });

};
