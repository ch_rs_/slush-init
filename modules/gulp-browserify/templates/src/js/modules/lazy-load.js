//----------------------------------*\
// Lazy loader
//----------------------------------*\

module.exports = function(element,options){

    elements = document.querySelectorAll(element);

    // Check for target element
    if( elements == null ) return;

    [].forEach.call(elements, function(element){

      element.addEventListener('click',function(event){
        event.preventDefault();

        var container = document.querySelector('.js-loadable-items');

        if(container !== null){
          container.classList.remove('is-hidden');
          element.remove();
          var images = container.querySelectorAll('[data-src]');

          [].forEach.call(images, function(image){
            image.setAttribute('src', image.getAttribute('data-src'));
            image.removeAttribute('data-src');
          });
        }

      });
    });

};
