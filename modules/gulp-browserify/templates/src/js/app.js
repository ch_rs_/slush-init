require('svg4everybody')();

require('./modules/lightbox.js')('.js-lightbox');

require('./modules/body-toggle.js')('[data-body-toggle]',{
  "attribute": "data-body-toggle"
});

var navHover = require('./modules/nav-hover.js');
new navHover('.js-nav');

require('./modules/lazy-load.js')('.js-load-more');
require('./modules/object-fit.js')('.js-fit-picture');
