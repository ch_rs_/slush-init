var error = require('./_error.js');

var watchify = require('watchify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var assign = require('lodash').assign;

module.exports = function (gulp, $, production, paths) {

  var customOpts = {
    app: {
      entries: [paths.public.app],
      debug: (!production)
    }
  };
  var opts = assign({}, watchify.args, customOpts.app);
  var b = watchify(browserify(opts));

  if(!production){
    b.on('update', bundle);
  }

  function bundle(){
    return b.bundle()
    .on('error',error)
    .pipe(source(paths.public.app))
    .on('error',error)
    .pipe(buffer())
    .pipe($.if(!production,$.sourcemaps.init({loadMaps: true})))
    .pipe($.if(production,$.uglify()))
    .pipe($.if(!production,$.sourcemaps.write()))
    .pipe($.rename('global.js'))
    .pipe($.size({title:"Javascript"}))
    .pipe(gulp.dest(paths.output))
    .pipe($.if(!production,$.browserSync.reload({stream: true})));
  };
  gulp.task('watchScripts', bundle);

  gulp.task('buildScripts', function () {
    return browserify(paths.public.app)
    .bundle()
    .on('error',error)
    .pipe(source('global.js'))
    .pipe(buffer())
    .pipe($.uglify())
    .pipe($.size({title:"Javascript"}))
    .pipe(gulp.dest(paths.output));
  });
}
