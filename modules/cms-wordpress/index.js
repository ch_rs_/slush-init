var gulp = require('gulp');
var https = require('https');
var async = require('async');
var plugins = require('gulp-load-plugins')();
var _ = require('lodash');

var ignoreFiles = ['.DS_Store'];

var wpPlugins = {
  'wp-sync-db': {
    name: 'WP Sync DB',
    url: 'https://github.com/wp-sync-db/wp-sync-db.git'
  }
};

module.exports = {
  name: 'wordpress',
  description: 'WordPress core, plugins and salts',
  group: '01-cms',
  templatesByDefault: false,
  prompts: [{
      type: 'checkbox',
      name: 'wpPlugins',
      message: 'Plugins to install',
      choices: _.map(wpPlugins, function(value, key){
        return {
          name: value.name,
          value: key
        }
      })
  }],
  extraTasks: function(answers){

    async.auto({
      salts: function(callback){

        console.log('Fetching salts');

        https.get('https://api.wordpress.org/secret-key/1.1/salt/', function (response) {
          response.setEncoding('utf8')
          response.on('data', function(salts){
            answers.salts = salts;
            callback(null, answers);
          })
          response.on('error', callback);
        });
      },
      templates: ['salts', function(result, callback){

        console.log('Copying templates');

        gulp.src([__dirname + '/templates/**'])
          .pipe(plugins.ignore(ignoreFiles))
          .pipe(plugins.template(result.salts))
          .pipe(plugins.conflict('./'))
          .pipe(gulp.dest('./'))
          .on('end', function(){
            callback(null);
          })
          .on('error', function(err){
            callback(err);
          })
          .resume();

      }],
      core: [function(callback){

        console.log('Downloading WP core');

        plugins.remoteSrc(['latest'], { base: 'https://wordpress.org/' })
          .pipe(plugins.decompress({strip: 1}))
          .pipe(plugins.ignore(ignoreFiles.concat(['wp-content', 'wp-content/**', '*-sample.php', 'readme.*'])))
          .pipe(plugins.conflict('./'))
          .pipe(gulp.dest('./www/wp'))
          .on('end', function(){
            callback(null);
          })
          .on('error', function(err){
            callback(err);
          })
          .resume();

      }],
      plugins: [function(callback){

        console.log('Downloading plugins');

        async.each(answers.wpPlugins,
        function(pluginName, callback){
          var plugin = wpPlugins[pluginName];
          plugins.git.clone(plugin.url, {args: './www/content/plugins/' + pluginName}, function(err) {
            if(err) console.log(err);
            callback();
          });
        },
        function(){
          callback(null);
        });

      }]
    }, function(err, results) {
      if(err) throw err;
      console.log('Wordpress setup completed')
    });

  }
}
