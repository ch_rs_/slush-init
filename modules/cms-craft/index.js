var gulp = require('gulp');

var prompts = [
  {type: 'input', name: 'name', message: 'Project name'},
  {type: 'confirm', name: 'finished', message: 'Continue?'}
];

module.exports = {
  name: "craft",
  description: "Craft core",
  group: "01-cms",
  prompts: [
    {
      type: 'checkbox',
      name: 'plugins',
      message: 'Plugins to install',
      choices: [{
        name: 'WP Sync DB',
        value: 'wp-sync-db',
        url: 'https://github.com/wp-sync-db/wp-sync-db.git'
      }]
  }]
};
