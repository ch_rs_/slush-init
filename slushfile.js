var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var slug = require('slug');
var async = require('async');
var _ = require('lodash');
var fullname = require('fullname');
var fs = require('fs');
slug.defaults.mode = 'rfc3986';
plugins.inquirer = require('inquirer');

var childProcess = require('child_process');

var modules = {};
var selectedModules = [];
var moduleDir = __dirname + '/modules';
var modFolders = fs.readdirSync(moduleDir).filter(function(a){ return a.indexOf('.') < 0 });

var ignoreFiles = ['.DS_Store'];

// Set global prompts
var prompts = [
  {
    type: 'input',
    name: 'name',
    message: 'Project name',
    validate: function(val){
      return (val !== '');
    }
  },
  {
    type: 'input',
    name: 'slug',
    message: 'Project slug',
    default: function(answers) {
      return slug(answers.name);
    },
    validate: function(val){
      return (val !== '');
    }
  },
  {
    type: 'input',
    name: 'author',
    message: 'Author',
    default: function(answers) {
      return fullname();
    },
    validate: function(val){
      return (val !== '');
    }
  },
  {
    type: 'input',
    name: 'tableprefix',
    message: 'Table prefix (no underscore)',
    default: function(answers) {
      return (answers.slug.split('-').length > 2) ? answers.slug.replace('-',' ').match(/\b(\w)/g).join('') : answers.slug.replace('-','').substring(0,3);
    },
    validate: function(val){
      return (val !== '');
    }
  }
];

gulp.task('default', function(done){

  childProcess.execFile('git', ['init'], function(err, stdout){
      if(err) throw err;
  });

  // Find all modules
  modFolders.forEach(function(mod){
    modules[mod] = require(moduleDir + '/' + mod + '/index.js');
    modules[mod].key = mod;
    modules[mod].dir = mod;
  });

  // Group modules and prepare prompts
  var groupedModules = _.groupBy(modules, 'group');
  var groupPrompt = _.map(groupedModules, function(group, key){
    return {
      type: 'checkbox',
      name: key,
      message: 'Select module(s) to include from group: ' + key + '',
      default: _.map(group, function(mod){
        if(mod.default){
          return mod.dir
        }
      }),
      choices: _.map(group, function(mod){
        return {
          value: mod.dir,
          name: mod.description
        }
      })
    };
  });
  groupPrompt = _.sortBy(groupPrompt, ['name']);

  log('Modules to include');

  // Prompt for modules to run
  plugins.inquirer.prompt(groupPrompt).then(function(answers){

      _.each(answers, function(a){
        a.forEach(function(a){
          // Add module questions to prompt array and add module to selectedModules
          prompts = prompts.concat(modules[a].prompts);
          selectedModules.push(modules[a]);
        });
      });

  }).then(function(){

    log('General settings');

    // Run all prompts
    plugins.inquirer.prompt(prompts).then(function(answers) {

      // Copy global files
      gulp.src([__dirname + '/global/**', __dirname + '/global/.*'])
        .pipe(plugins.ignore(ignoreFiles))
        .pipe(plugins.template(answers))
        .pipe(plugins.conflict('./'))
        .pipe(gulp.dest('./'))
        .on('end', function () {

          // Merge together devDependencies for each module
          var deps = {};
          selectedModules.forEach(function(mod){
            _.merge(deps, mod.devDependencies);
          });

          // Apply the devDependencies to package.json
          applyPackageJSON(deps, function(){

            // Run all selected modules
            selectedModules.forEach(function(mod){

              if(mod.templatesByDefault === undefined || mod.templatesByDefault === true){

                console.log("Copying template files for " + mod.name);

                gulp.src([__dirname + '/modules/' + mod.key + '/templates/**'])
                  .pipe(plugins.ignore(ignoreFiles))
                  .pipe(plugins.template(answers))
                  .pipe(plugins.conflict('./'))
                  .pipe(gulp.dest('./'))
                  .on('end', function(){
                    if(mod.extraTasks !== undefined){
                        mod.extraTasks(answers);
                    }
                  })
                  .on('error', function(err){
                    throw err;
                  })
                  .resume();
              }
              else if(mod.extraTasks !== undefined) {

                console.log("Initializing " + mod.name);
                console.log("Merging module package.json");

                mod.extraTasks(answers);
              }
            });

          })

          done();
        })
        .resume();

    });
  });
});

function applyPackageJSON(deps, callback) {

  gulp.src('./package.json')
    .pipe(plugins.jsonEditor({
      'devDependencies': deps
    }))
    .pipe(gulp.dest('./'))
    .on('end', function(){
      callback();
    })
    .on('error', function(err){
      throw err;
    })
    .resume();


}


function log(msg){
  var sep = plugins.util.colors.yellow('======');
  console.log('');
  console.log(sep, msg, sep);
  console.log('');
}
