# Site generator

Customisable website generator.

* Powered by Gulp
* Uses modules for easy customisation
* Includes default files for git, stylelint, etc.
* Optionally downloads and sets up WordPress and plugins
* Optionally adds Docker configuration for local development

## Usage

This generator uses [Slush](https://slushjs.github.io/), which allows Gulp to be used as a scaffolding system. Install the `slush` package globally, then run `npm link` in the root of this directory to symlink it as a global package. Then you can run `slush init` in an empty directory to get started.

## Modules

The generator will scan the `/modules` directory and import every module with an `index.js` file. If there is a `template` subdirectory within the module, it will automatically copy the enclosed files to the route of your project. This is the easiest way to add a set of optional files to the generator. Any files copied this way will be parsed by lodash, so template variavles (`<%= example %>`) can be used.

You can also set the `prompts` option to add module-specific prompts to the stack, which will appear after the general prompts and will be passed back as variables to your templates.

Setting the `group` property allows modules to be prompted in specific groups when the generator is initialised. For example, the `group` property for the WordPress module is `01-cms`, which means WordPress and Craft appear as options for the same prompt and they appear first.

## To do

* Allow customisable web root
* Add Gulp SCSS module
* Add Timber and Standard WP themes
* Add Craft CMS module
* Move Gulp paths object into the relevant modules
* Make CSS/JS partials optional
* Add Keystone CMS module
* Reduce number of callbacks in slushfile